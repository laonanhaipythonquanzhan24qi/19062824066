#!usr/bin/env python
# *- coding:utf-8 -*-
# Andy Create @ 11/19/2019 3:39 PM


from flask import Flask

from .views.account import account
from .views.book import book


def create_app():
    app = Flask(__name__, static_folder='static', template_folder='templates')
    app.secret_key = 'flaskexcelproject'

    app.register_blueprint(account)
    app.register_blueprint(book)

    return app
