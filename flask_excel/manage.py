#!usr/bin/env python
# *- coding:utf-8 -*-
# Andy Create @ 11/19/2019 3:37 PM
import os

from flask_excel import create_app



app = create_app()

if __name__ == '__main__':
    app.run(debug=True)