#!usr/bin/env python
# *- coding:utf-8 -*-
# Andy Create @ 10/22/2019 4:34 PM

from django import template
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.http.request import QueryDict

register = template.Library()

@register.simple_tag
def show_pri_pub_option(request):
    # render private, publish option
    path = request.path
    if path == reverse('sales:customer'):
        return mark_safe('<option value="reverse_gs">公户转私户</option>')
    else:
        return mark_safe('<option value="reverse_sg">私户转公户</option>')

# 拼接路径,编辑之后跳转回原页面
@register.simple_tag
def reverse_url(url_name,id,request):
    # encode the url with search params
    # /editcustomer/3/?next=/customers/?page=4
    path = request.get_full_path()
    query_dict_obj = QueryDict(mutable=True)  # make it mutable to change it
    query_dict_obj['next'] = path #
    encode_url = query_dict_obj.urlencode() #next=/customers/?search_field=qq__contains&keyword=1&page=4

    prefix_path = reverse(url_name,args=(id,)) #/editcustomer/3/

    full_path = prefix_path + '?' +  encode_url

    return full_path
