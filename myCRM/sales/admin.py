from django.contrib import admin

from sales.models import *


class UserInfoAdmin(admin.ModelAdmin):
    list_display = ['username', 'telephone', 'email', 'is_active']


class DepartmentAdmin(admin.ModelAdmin):
    list_display = ['name', ]


class CustomerAdmin(admin.ModelAdmin):
    list_display = ['qq_name', 'qq', 'phone', 'status', ]


class CampusesAdmin(admin.ModelAdmin):
    list_display = ['name', 'address']


class ClassListAdmin(admin.ModelAdmin):
    list_display = ['course', 'semester', 'campuses', 'price', 'memo', 'start_date', 'graduate_date',
                    'class_type']


class ConsultRecordAdmin(admin.ModelAdmin):
    list_display = ['customer', 'status', 'consultant', 'date', 'delete_status']

class EnrollmentAdmin(admin.ModelAdmin):
    list_display = ['customer', 'school', 'enrolment_class', 'enrolled_date']

class CourseRecordAdmin(admin.ModelAdmin):
    list_display = ['course_title',]

class StudyRecordAdmin(admin.ModelAdmin):
    list_display = ['student','score', 'attendance',]

admin.site.register(UserInfo, UserInfoAdmin)
admin.site.register(Department, DepartmentAdmin)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(Campuses, DepartmentAdmin)
admin.site.register(ClassList, ClassListAdmin)
admin.site.register(Enrollment, EnrollmentAdmin)
admin.site.register(ConsultRecord, ConsultRecordAdmin)
admin.site.register(CourseRecord, CourseRecordAdmin)
admin.site.register(StudyRecord, StudyRecordAdmin)
