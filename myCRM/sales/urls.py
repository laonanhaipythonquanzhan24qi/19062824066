#!usr/bin/env python
# *- coding:utf-8 -*-
# Andy Create @ 10/18/2019 11:14 AM

from django.conf.urls import url
from sales.views import *

urlpatterns = [
    url(r'^login/$', login, name='login'),
    url(r'^logout/$', logout, name='logout'),
    url(r'^register/$', register, name='register'),
    url(r'^home/$', CustomerView.as_view(), name='home'),
    url(r'^customer/$', CustomerView.as_view(), name='customer'),
    url(r'^add_customer/$', add_edit_customer, name='add_customer'),
    url(r'^edit_customer/(?P<pk>\d+)/$', add_edit_customer, name='edit_customer'),
    url(r'^consult_record/$', ConsultRecordView.as_view(), name='consult_record'),
    url(r'^add_consult_record/$', add_edit_consult_record, name='add_consult_record'),
    url(r'^edit_consult_record/(?P<pk>\d+)$', add_edit_consult_record, name='edit_consult_record'),
    url(r'^enrollment/$', EnrollmentView.as_view(), name='enrollment'),
    url(r'^add_enrollment/$', add_edit_enrollment, name='add_enrollment'),
    url(r'^edit_enrollment/(?P<pk>\d+)$', add_edit_enrollment, name='edit_enrollment'),
    url(r'^course_record/$', CourseRecordView.as_view(), name='course_record'),
    url(r'^add_course_record/$', add_edit_course_record, name='add_course_record'),
    url(r'^edit_course_record/(?P<pk>\d+)$', add_edit_course_record, name='edit_course_record'),
    url(r'^study_record/(?P<course_id>\d+)$', StudyRecordView.as_view(), name='study_record'),
    url(r'^add_study_record/$', add_edit_study_record, name='add_study_record'),
    url(r'^edit_study_record/(?P<pk>\d+)$', add_edit_study_record, name='edit_study_record'),
]
