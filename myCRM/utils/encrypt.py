#!usr/bin/env python
# *- coding:utf-8 -*-
# Andy Create @ 10/21/2019 6:48 PM

import hashlib


def md5_encrypt(value):
    # 将传入的数据加盐，加密
    secret_key = 'username'.encode('uf8-8')

    res = hashlib.md5(secret_key)
    res.update(value.encode('utf-8'))
    return res.hexdigest()
