#!usr/bin/env python
# *- coding:utf-8 -*-
# Andy Create @ 11/13/2019 12:20 PM
import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
PLUGIN_DICT = {
    "board": "lib.plugins.board.Board",
    "disk": "lib.plugins.disk.Disk",
    "memory": "lib.plugins.memory.Memory",
    "network":"lib.plugins.network.Network",
    "cpu":"lib.plugins.cpu.Cpu",
}

SSH_PORT =5188
SSH_USER = "root"
SSH_PRIVATE_KEY_PATH = r'files/id_rsa'
THREAD_POOL_SIZE = 10

DEBUG = True
MODE = "SSH" # SALT/SSH
LOG_FILE_PATH = os.path.join(BASE_DIR,'logs/cmdb.log')

