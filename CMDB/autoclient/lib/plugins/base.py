#!usr/bin/env python
# *- coding:utf-8 -*-
# Andy Create @ 11/13/2019 5:44 PM

class BasePlugin(object):
    def process(self, hostname, ssh_func):
        raise NotImplementedError("子类%s必须实现process方法"%self.__class__.__name__)

