#!usr/bin/env python
# *- coding:utf-8 -*-
# Andy Create @ 11/13/2019 12:20 PM

from settings import PLUGIN_DICT
import importlib

def get_server_info(hostname, ssh_func):
    """
    :param hostname: 要远程操作的主机名
    :param ssh_func: 执行远程操作的方法
    :return: 执行命令后得到的信息
    """

    info_dict = {}
    for key, path in PLUGIN_DICT.items():
        module_path, class_name = path.rsplit('.', maxsplit=1)
        # 根据字符串的形式去导入模块 “lib.plugins.board"
        module = importlib.import_module(module_path)
        # 去模块找到类
        try:
            cls = getattr(module, class_name)
        except Exception:
            raise ImportError('模块获取失败')
        # 对类型实例化
        obj = cls()
        # 执行对象的process方法
        result = obj.process(hostname, ssh_func)
        info_dict[key] = result

    return info_dict