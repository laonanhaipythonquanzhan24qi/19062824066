#!usr/bin/env python
# *- coding:utf-8 -*-
# Andy Create @ 11/14/2019 12:36 PM

class BaseResponse(object):
    def __init__(self):
        self.status = True
        self.data = None
        self.error = None

    @property
    def dict(self):
        return self.__dict__




