from django.contrib import admin
from api import models

# Register your models here.
admin.site.register(models.Disk)
admin.site.register(models.Depart)
admin.site.register(models.Server)
admin.site.register(models.Memory)
admin.site.register(models.Network)
