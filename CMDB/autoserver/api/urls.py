#!usr/bin/env python
# *- coding:utf-8 -*-
# Andy Create @ 11/13/2019 10:54 AM
from django.conf.urls import url, include

from api.views import *

urlpatterns = [
    url(r'server/$', ServerView.as_view()),
]
