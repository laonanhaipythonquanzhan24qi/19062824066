from django.test import TestCase

# Create your tests here.
"""
    name = models.CharField(verbose_name='网卡名', max_length=32)
    up = models.BooleanField(verbose_name='状态')
    hwaddr = models.CharField(verbose_name='硬件地址', max_length=32)
    address = models.CharField(verbose_name='ip', max_length=32)
    netmask = models.CharField(verbose_name='netmask', max_length=32)
    broadcast = models.CharField(verbose_name='broadcast', max_length=32)

"""
network_info = {'eth0': {'up': True, 'hwaddr': '00:16:3c:78:45:2c',
                 'inet': [
                     {'address': '107.174.101.162', 'netmask': '255.255.255.224', 'broadcast': '107.174.101.191'}]}}

data = {}
for key, value in network_info.items():
    data['name'] = key
    data['up'] = value['up']
    data['hwaddr'] = value['hwaddr']
    data['address'] = value['inet'][0]['address']
    data['netmask'] = value['inet'][0]['netmask']
    data['broadcast'] = value['inet'][0]['broadcast']

print(data)
