from django.db import models


# Create your models here.

class Depart(models.Model):
    """ 部门表 """
    title = models.CharField(verbose_name='部门', max_length=32)


class Server(models.Model):
    """服务器表"""
    status_choices = (
        (1, '上线'),
        (2, '下线'),
    )
    status = models.IntegerField(verbose_name='状态', choices=status_choices, default=1)

    depart = models.ForeignKey(verbose_name='部门', to='Depart')

    host_name = models.CharField(verbose_name='主机名', max_length=32)
    last_check_date = models.DateField(verbose_name='最近采集时间', null=True, blank=True)

    # board
    sn = models.CharField(verbose_name='SN号', max_length=64, null=True, blank=True)
    model = models.CharField(verbose_name='SN号', max_length=64, null=True, blank=True)
    manufacturer = models.CharField(verbose_name='SN号', max_length=64, null=True, blank=True)

    # CPU
    cpu_count = models.IntegerField(verbose_name='CPU逻辑核数', null=True, blank=True)
    cpu_physical_count = models.IntegerField(verbose_name='CPU物理核数', null=True, blank=True)
    cpu_model = models.CharField(verbose_name='CPU型号', max_length=64, null=True, blank=True)

    def __str__(self):
        return self.host_name


# class Board(models.Model):
#     server = models.OneToOneField(to='Server', on_delete=models.CASCADE, to_field='id')
#     manufacturer = models.CharField(verbose_name='制造商', max_length=64)
#     model = models.CharField(verbose_name='模式', max_length=64)
#     sn = models.CharField(verbose_name='序列号', max_length=64)


class Disk(models.Model):
    server = models.ForeignKey(to='Server', to_field='id', on_delete=models.CASCADE)
    slot = models.CharField(verbose_name='槽位', max_length=32)
    pd_type = models.CharField(verbose_name='类型', max_length=32)
    capacity = models.CharField(verbose_name='容量', max_length=32)
    model = models.CharField(verbose_name='模型', max_length=64)


class Memory(models.Model):
    server = models.ForeignKey(to='Server', to_field='id', on_delete=models.CASCADE)
    slot = models.CharField(verbose_name='槽', max_length=2)
    capacity = models.CharField(verbose_name='容量', max_length=10)
    model = models.CharField(verbose_name='模式', max_length=20)
    speed = models.CharField(verbose_name='速度', max_length=10, blank=True, null=True)
    manufacturer = models.CharField(verbose_name='生产商', max_length=20, blank=True, null=True)
    sn = models.CharField(verbose_name='序列号', max_length=32, blank=True, null=True)


class Network(models.Model):
    server = models.ForeignKey(to='Server', to_field='id', on_delete=models.CASCADE)
    name = models.CharField(verbose_name='网卡名', max_length=32)
    up = models.BooleanField(verbose_name='状态')
    hwaddr = models.CharField(verbose_name='硬件地址', max_length=32)
    address = models.CharField(verbose_name='ip', max_length=32)
    netmask = models.CharField(verbose_name='netmask', max_length=32)
    broadcast = models.CharField(verbose_name='broadcast', max_length=32)


class Record(models.Model):
    """ 资产更变记录 """
    server = models.ForeignKey(verbose_name='服务器', to='Server')
    content = models.TextField(verbose_name='变更内容')
    date = models.DateTimeField(verbose_name='时间', auto_now_add=True)
