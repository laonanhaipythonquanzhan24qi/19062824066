# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2019-11-18 13:53
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_auto_20191116_0243'),
    ]

    operations = [
        migrations.CreateModel(
            name='Depart',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=32, verbose_name='部门')),
            ],
        ),
        migrations.RemoveField(
            model_name='board',
            name='server',
        ),
        migrations.AddField(
            model_name='server',
            name='cpu_count',
            field=models.IntegerField(blank=True, null=True, verbose_name='CPU逻辑核数'),
        ),
        migrations.AddField(
            model_name='server',
            name='cpu_model',
            field=models.CharField(blank=True, max_length=64, null=True, verbose_name='CPU型号'),
        ),
        migrations.AddField(
            model_name='server',
            name='cpu_physical_count',
            field=models.IntegerField(blank=True, null=True, verbose_name='CPU物理核数'),
        ),
        migrations.AddField(
            model_name='server',
            name='manufacturer',
            field=models.CharField(blank=True, max_length=64, null=True, verbose_name='SN号'),
        ),
        migrations.AddField(
            model_name='server',
            name='model',
            field=models.CharField(blank=True, max_length=64, null=True, verbose_name='SN号'),
        ),
        migrations.AddField(
            model_name='server',
            name='sn',
            field=models.CharField(blank=True, max_length=64, null=True, verbose_name='SN号'),
        ),
        migrations.DeleteModel(
            name='Board',
        ),
        migrations.AddField(
            model_name='server',
            name='depart',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='api.Depart', verbose_name='部门'),
            preserve_default=False,
        ),
    ]
