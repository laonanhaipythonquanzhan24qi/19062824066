from django.shortcuts import render


# Create your views here.

def home(request):
    my_list = [
        'first',
        'second',
        'third',
    ]
    return render(request, 'home.html', {'my_list': my_list})
