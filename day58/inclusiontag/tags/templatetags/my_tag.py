#!usr/bin/env python
# *- coding:utf-8 -*-
# Andy Create @ 9/26/2019 4:45 PM

from django import template

register = template.Library()


@register.inclusion_tag('list.html')
def show_list(my_list):
    new_list = []
    for l in my_list:
        if len(l) > 5:
            l = l[:5]
        new_list.append(l)
    return {'my_list':new_list}
