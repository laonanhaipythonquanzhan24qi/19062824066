from django.shortcuts import render


# Create your views here.

def home(request):
    my_list = [
        'first',
        'second',
        'third',
    ]
    context = {
        'my_list': my_list,
    }
    return render(request, 'home.html', context)
