#!usr/bin/env python
# *- coding:utf-8 -*-
# Andy Create @ 10/12/2019 9:08 AM


from django.utils.deprecation import MiddlewareMixin
from django.shortcuts import reverse, redirect


class Own_Auth(MiddlewareMixin):
    white_list = ['/login/', '/admin/', '/admin/login/', ]

    def process_request(self, request):
        next_url = request.path
        is_login = request.session.get('is_login', None)
        if is_login or next_url in self.white_list:
            return None
        else:
            return redirect(reverse("account:login"))
