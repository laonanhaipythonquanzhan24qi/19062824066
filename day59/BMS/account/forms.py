#!usr/bin/env python
# *- coding:utf-8 -*-
# Andy Create @ 10/12/2019 9:30 AM
from django import forms
from django.contrib.auth.models import User

class LoginForm(forms.Form):
    username = forms.CharField(required=True)
    password = forms.CharField(required=True)

    def clean_username(self):
        username = self.cleaned_data['username']
        username_exists = User.objects.filter(username=username).exists()
        if not username_exists:
            self.add_error('username', '用户名不存在')
        else:
            return username

    def clean_password(self):
        password = self.cleaned_data['password']
        if len(password) < 8:
            self.add_error('password', '密码不正确')
        else:
            return password
