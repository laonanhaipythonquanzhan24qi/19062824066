from django.db import models
from django.utils import timezone


# Create your models here.


class Author(models.Model):
    # 作者
    name = models.CharField(verbose_name='姓名', max_length=32)
    age = models.IntegerField(verbose_name='年龄')
    author_detail = models.OneToOneField(to="AuthorDetail", verbose_name='作者详情')

    class Meta:
        verbose_name_plural = verbose_name = '作者'

    def __str__(self):
        return self.name


class AuthorDetail(models.Model):
    # 作者详情
    birthday = models.DateField(verbose_name='生日')
    telephone = models.CharField(verbose_name='电话', max_length=11)
    addr = models.CharField(verbose_name='地址', max_length=64)

    class Meta:
        verbose_name_plural = verbose_name = '作者详情'

    def __str__(self):
        return 'Ta生于{0}年{1}月{2}'.format(self.birthday.year, self.birthday.month, self.birthday.day)


class Press(models.Model):
    # 出版社
    name = models.CharField(verbose_name='社名', max_length=32)
    city = models.CharField(verbose_name='所在城市', max_length=32)

    class Meta:
        verbose_name_plural = verbose_name = '出版社'

    def __str__(self):
        return self.name


class Book(models.Model):
    # 书籍
    name = models.CharField(verbose_name='书名', max_length=20, )
    price = models.DecimalField(
        verbose_name='价格', max_digits=5, decimal_places=2)
    publish_date = models.DateField(verbose_name='出版日期', default=timezone.now)
    press = models.ForeignKey(to='Press', verbose_name='出版社', related_name='press_book')
    author = models.ManyToManyField(to='Author', verbose_name='作者', related_name='author_book')

    class Meta:
        verbose_name_plural = verbose_name = '书籍'

    def __str__(self):
        return self.name
