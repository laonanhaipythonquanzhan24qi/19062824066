from django.contrib import admin
from .models import *


# Register your models here.


class BookAdmin(admin.ModelAdmin):
    list_display = ['name', 'price', 'press']


class AuthorAdmin(admin.ModelAdmin):
    list_display = ['name', ]


class PressAdmin(admin.ModelAdmin):
    list_display = ['name', ]


class AuthorDetailAdmin(admin.ModelAdmin):
    list_display = ['birthday', ]


admin.site.register(Book, BookAdmin)
admin.site.register(Author, AuthorAdmin)
admin.site.register(Press, PressAdmin)
admin.site.register(AuthorDetail, AuthorDetailAdmin)
