#!usr/bin/env python
# *- coding:utf-8 -*-
# Andy Create @ 10/7/2019 3:03 PM

from django import forms
from book.models import *


class AuthorDetailForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(AuthorDetailForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})

    class Meta:
        model = AuthorDetail
        fields = "__all__"
        widgets = {
            'birthday': forms.TextInput(attrs={'type': 'date'}),
        }


class AuthorForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(AuthorForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control', })

    class Meta:
        model = Author
        fields = '__all__'


class BookForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(BookForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})

    class Meta:
        model = Book
        fields = '__all__'

        widgets = {
            'publish_date': forms.TextInput(attrs={'type': 'date'}),
        }


class PressForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(PressForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})

    class Meta:
        model = Press
        fields = "__all__"
