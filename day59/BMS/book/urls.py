#!usr/bin/env python
# *- coding:utf-8 -*-
# Andy Create @ 9/28/2019 3:34 PM
from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'book_show/', book_show, name='book_show'),
    url(r'book_add/', book_add, name='book_add'),
    url(r'book_del/(?P<id>\d+)/', book_del, name='book_del'),
    url(r'book_edit/(?P<id>\d+)/', book_edit, name='book_edit'),
    url(r'authordetail_show/', author_detail_show, name='author_detail_show'),
    url(r'authordetail_add/', author_detail_add, name='author_detail_add'),
    url(r'authordetail_edit/(?P<id>\d+)', author_detail_edit, name='author_detail_edit'),
    url(r'authordetail_del/(?P<id>\d+)', author_detail_del, name='author_detail_del'),
    url(r'author_show/', author_show, name='author_show'),
    url(r'author_add/', author_add, name='author_add'),
    url(r'author_edit/(?P<id>\d+)', author_edit, name='author_edit'),
    url(r'author_del/(?P<id>\d+)', author_del, name='author_del'),
    url(r'press_show/', press_show, name='press_show'),
    url(r'press_add/', press_add, name='press_add'),
    url(r'press_edit/(?P<id>\d+)', press_edit, name='press_edit'),
    url(r'press_del/(?P<id>\d+)', press_del, name='press_del'),
]
