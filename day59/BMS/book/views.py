from django.shortcuts import render, redirect
from django.db.models import Count
from django.http import JsonResponse
from .models import *
from book.forms import *


# Create your views here.
def author_detail_add(request):
    # 添加作者详情
    if request.method == 'GET':
        author_detail_form = AuthorDetailForm()
        context = {
            'author_detail_form': author_detail_form,
        }
        return render(request, 'book/author_detail_add.html', context)
    else:
        added_author_detail = AuthorDetailForm(request.POST)
        if added_author_detail.is_valid():
            added_author_detail.save()
        else:
            context = {
                'author_detail_form': added_author_detail,
            }
            return render(request, 'book/author_detail_add.html', context)
        return redirect('book:author_detail_show')


def author_detail_show(request):
    # 展示作者详情
    authordetails = AuthorDetail.objects.all()
    context = {
        'authordetails': authordetails,
    }
    return render(request, 'book/author_detail_show.html', context)


def author_detail_edit(request, id):
    # 编辑作者详情
    author_detail = AuthorDetail.objects.get(id=id)
    if request.method == 'GET':
        edit_author_detail = AuthorDetailForm(instance=author_detail)
        context = {
            'author_detail_form': edit_author_detail,
            'author_detail': author_detail,
        }
        return render(request, 'book/author_detail_edit.html', context)
    else:
        updated_author_detail_form = AuthorDetailForm(request.POST, instance=author_detail)
        if updated_author_detail_form.is_valid():
            updated_author_detail_form.save()
            return redirect('book:author_detail_show')
        else:
            context = {
                'author_detail_form': updated_author_detail_form,
                'author_detail': author_detail,
            }
            return render(request, 'book/author_detail_edit.html', context)


def author_detail_del(request, id):
    # 删除作者详情
    author_detail = AuthorDetail.objects.get(id=id)
    author_detail.delete()
    return redirect('book:author_detail_show')


def author_show(request):
    # 展示所有作者
    authors = Author.objects.all()
    context = {
        'authors': authors,
    }
    return render(request, 'book/author_show.html', context)


def author_add(request):
    # 添加作者
    # TODO, 处理当作者详情冲突时的情况
    # TODO, 添加完作者怎么保存已经填写的姓名，年龄信息，并填充到作者添加页面

    # 传入所有的作者详情，供选择
    authordetails = AuthorDetail.objects.all()

    if request.method == 'GET':
        authors = Author.objects.all()

        context = {
            'authors': authors,
            'authordetails': authordetails,
        }
        return render(request, 'book/author_add.html', context)
    else:
        name = request.POST.get('name')
        age = request.POST.get('age')
        author_detail_id = request.POST.get('authordetail')
        new_author_detail = AuthorDetail.objects.get(id=author_detail_id)
        new_author, _ = Author.objects.update_or_create(name=name, age=age, author_detail=new_author_detail)
        return redirect('book:author_show')


def author_edit(request, id):
    # 编辑作者
    author = Author.objects.get(id=id)
    if request.method == 'GET':
        author_form = AuthorForm(instance=author)
        context = {
            'author_form': author_form,
            'author': author,
        }
        return render(request, 'book/author_edit.html', context)
    else:
        updated_author = AuthorForm(request.POST, instance=author)
        if updated_author.is_valid():
            updated_author.save()
        else:
            context = {
                'author': author,
                'author_form': updated_author,
            }
            return render(request, 'book/author_edit.html', context)
        return redirect('book:author_show')


def author_del(request, id):
    # 删除作者
    author = Author.objects.get(id=id)
    author.delete()
    return redirect('book:author_show')


def press_show(request):
    press_list = Press.objects.annotate(book_count=Count('press_book'))
    context = {
        'press_list': press_list,
    }

    return render(request, 'book/press_show.html', context)


def press_edit(request, id):
    press = Press.objects.get(id=id)
    if request.method == 'GET':
        press_form = PressForm(instance=press)
        context = {
            'press_form': press_form,
            'press': press,
        }
        return render(request, 'book/press_edit.html', context)
    else:
        updated_press = PressForm(request.POST, instance=press)
        if updated_press.is_valid():
            press.save()
        else:
            context = {
                'press': press,
                'press_form': updated_press,
            }
            return render(request, 'book/press_edit.html', context)
        return redirect('book:press_show')


def press_del(request, id):
    press = Press.objects.get(id=id)
    press.delete()
    return redirect('book:press_show')


def press_add(request):
    # 添加出版社
    if request.method == 'GET':
        press_form = PressForm()
        context = {
            'press_form': press_form,
        }
        return render(request, 'book/press_add.html', context)
    else:
        name = request.POST.get('name')
        city = request.POST.get('city')
        new_press, _ = Press.objects.update_or_create(name=name, city=city)
        return redirect('book:press_show')


def book_show(request):
    # 展示所有书籍
    books = Book.objects.all()
    context = {
        'books': books,
    }
    return render(request, 'book/book_show.html', context)


def book_add(request):
    # 添加书籍
    if request.method == 'GET':
        book_form = BookForm()
        context = {
            'book_form': book_form,
        }
        # authors = Author.objects.all()
        # press_list = Press.objects.all()
        # context = {
        #     'authors': authors,
        #     'press_list': press_list,
        # }
        return render(request, 'book/book_add.html', context)

    else:
        name = request.POST.get('name')
        price = request.POST.get('price')
        publish_date = request.POST.get('publish_date')

        press_id = request.POST.get('press')  # 出版社id
        press_obj = Press.objects.get(id=int(press_id))  # 出版社对象
        new_book, _ = Book.objects.update_or_create(
            name=name,
            price=price,
            publish_date=publish_date,
            press=press_obj,
        )
        # 获取作者列表的 id列表
        author_list = request.POST.getlist('author')
        new_book.author.add(*author_list)  # 添加作者， 星星打散
        return redirect('book:book_show')


def book_del(request, id):
    # 删除书籍
    book = Book.objects.get(pk=id)
    if book:
        book.delete()
        data = {'status': 1}
    else:
        data = {'status': 0}

    return JsonResponse(data)


def book_edit(request, id):
    # # 编辑书籍
    book = Book.objects.get(id=id)
    if request.method == 'GET':
        book_obj = BookForm(instance=book)
        context = {
            'book_obj': book_obj,
            'book': book,
        }
        return render(request, 'book/book_edit.html', context)
    else:
        updated_book = BookForm(request.POST, instance=book)
        if updated_book.is_valid():
            updated_book.save()
        else:
            context = {
                'book_obj': updated_book,
                'book': book,
            }
            return render(request, 'book/book_edit.html', context)
        return redirect('book:book_show')
