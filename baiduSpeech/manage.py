#!usr/bin/env python
# *- coding:utf-8 -*-
# Author: Andy

from flask import Flask, render_template

app = Flask(__name__, template_folder='templates')


@app.route('/index', )
def index():

    return render_template('index.html', )


if __name__ == '__main__':
    app.run()
